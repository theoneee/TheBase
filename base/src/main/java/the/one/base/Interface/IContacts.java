package the.one.base.Interface;

public interface IContacts {

    String getLogo();

    String getName();

    String getPinYin();

    String getFirstPinYin();
}
