package the.one.base.widge;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import the.one.base.Interface.ImageSnap;
import the.one.base.R;
import the.one.base.util.ImagePreviewUtil;
import the.one.base.util.glide.GlideUtil;

/**
 * 描述：
 * 作者：HMY
 * 时间：2016/5/12
 */
public class NineGridView extends NineGridLayout {

    private static final String TAG = "NineGridView";

    protected int getRatio() {
        return 3;
    }

    protected int getMaxHeight() {
        return 600;
    }

    public NineGridView(Context context) {
        super(context);
    }

    public NineGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected boolean displayOneImage(final RatioImageView imageView, final String url,int width,int height, final int parentWidth) {
        if (width > 0 && height > 0 && parentWidth > 0) {
            loadOneImage(imageView, url, width, height, parentWidth);
        } else {
            Glide.with(mContext).asBitmap().load(url).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(@NonNull Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {
                    loadOneImage(imageView, url, bitmap.getWidth(), bitmap.getHeight(), parentWidth);
                }
            });
        }
        return false;
    }

    private void loadOneImage(RatioImageView imageView, String url, int width, int height, int parentWidth) {
        if (width == 0) return;
        int[] sizes = getAutoSize(width, height, parentWidth, getRatio(), getMaxHeight());
        int newW = sizes[0];
        int newH = sizes[1];
        setOneImageLayoutParams(imageView, newW, newH);
        Glide.with(getContext())
                .load(url)
                .transition(new DrawableTransitionOptions().crossFade())// 渐入渐出效果
                .apply(new RequestOptions()
                        .placeholder(R.drawable.pa_shape)
                        .override(newW, newH)
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(imageView);
    }

    public static int[] getAutoSize(int w, int h, int parentWidth, int ratio, int maxHeight) {
        int[] auto = new int[2];
        int newW;
        int newH;
        if (h > w * ratio) {//h:w = 5:3
            newW = parentWidth / 2;
            newH = newW * 5 / 3;
        } else if (h < w) {//h:w = 2:3
            newW = parentWidth * 2 / 3;
            newH = newW * 2 / 3;
        } else {//newH:h = newW :w
            newW = parentWidth / 2;
            newH = h * newW / w;
        }
        if (newH > maxHeight)
            newH = maxHeight;
        auto[0] = newW;
        auto[1] = newH;
        return auto;
    }

    @Override
    protected void displayImage(RatioImageView imageView, String url) {
        GlideUtil.load(mContext,url, imageView);
    }

    @Override
    protected void onClickImage(int position, View view, String url, ArrayList<ImageSnap> urlList) {
        ImagePreviewUtil.start((Activity) mContext, urlList, position);
    }

}
