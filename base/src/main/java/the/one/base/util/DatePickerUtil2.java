package the.one.base.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;

import com.qmuiteam.qmui.util.QMUIResHelper;

import java.util.Date;

import androidx.annotation.ColorInt;
import the.one.base.R;
import the.one.base.widge.datePicker.DateTimePicker;


/**
 * @author The one
 * @date 2018/10/25 0025
 * @describe 日期选择
 * @email 625805189@qq.com
 * @remark
 */
public class DatePickerUtil2 {

    @SuppressLint("StaticFieldLeak")
    private static DatePickerUtil2 datePickerUtil;

    public static DatePickerUtil2 getInstance() {
        if (null == datePickerUtil)
            datePickerUtil = new DatePickerUtil2();
        return datePickerUtil;
    }

    private DateTimePicker.Builder TBuilder;
    private DateTimePicker mTimePicker;

    public DatePickerUtil2 initTimePicker(Context context, DateTimePicker.ResultHandler resultHandler) {
        initTimePicker(context, getBuilder(context),DateUtil.getYearDate(1960), DateUtil.getYearDate(2030),resultHandler);
        return this;
    }

    public DatePickerUtil2 initTimePicker(Context context, Date start, Date end, DateTimePicker.ResultHandler resultHandler) {
        initTimePicker(context, getBuilder(context),start, end,resultHandler);
        return this;
    }

    public DatePickerUtil2 initTimePicker(Context context, DateTimePicker.Builder builder, Date start, Date end, DateTimePicker.ResultHandler resultHandler) {
        TBuilder = builder;
        mTimePicker = new DateTimePicker(context, resultHandler, start, end, TBuilder);
        return this;
    }

    public DateTimePicker.Builder getBuilder(Context context) {
        return getBuilder(context,DateTimePicker.ShowType.DAY);
    }

    public DateTimePicker.Builder getBuilder(Context context,DateTimePicker.ShowType type) {
        return getBuilder(context,"确定","取消","选择日期",context.getResources().getColor(R.color.qmui_config_color_blue),
                Color.RED,0xFF999999,QMUIResHelper.getAttrColor(context, R.attr.app_skin_primary_color),true,type);
    }

    public DateTimePicker.Builder getBuilder(Context context, String ok, String cancel, String title,@ColorInt int okColor,@ColorInt int cancelColor,@ColorInt int titleColor,@ColorInt int selectColor,
                                             boolean keepSelected, DateTimePicker.ShowType type) {
        return new DateTimePicker.Builder(context)
                .setOk(ok)
                .setCancel(cancel)
                .setTitle(title)
                .setCancelTextColor(cancelColor)
                .setOkTextColor(okColor)
                .setTitleTextColor(0xFF999999)
                .setKeepLastSelected(keepSelected)
                .setSelectedTextColor(titleColor)
                .setShowType(type);
    }

    public void show(Date date) {
        show("选择日期", date);
    }

    public void show(String title) {
        show(title, new Date());
    }

    public void show(String title, Date date) {
        TBuilder.setTitle(title);
        mTimePicker.show(date);
    }

}
