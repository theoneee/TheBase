package the.one.demo.ui.fragment

import android.graphics.Color
import android.view.View
import com.qmuiteam.qmui.util.QMUIResHelper
import com.qmuiteam.qmui.widget.roundwidget.QMUIRoundButton
import the.one.base.ui.fragment.BaseFragment
import the.one.base.ui.presenter.BasePresenter
import the.one.base.util.DateUtil
import the.one.base.widge.datePicker.DateTimePicker
import the.one.demo.R
import java.util.*

//  ┏┓　　　┏┓
//┏┛┻━━━┛┻┓
//┃　　　　　　　┃
//┃　　　━　　　┃
//┃　┳┛　┗┳　┃
//┃　　　　　　　┃
//┃　　　┻　　　┃
//┃　　　　　　　┃
//┗━┓　　　┏━┛
//    ┃　　　┃                  神兽保佑
//    ┃　　　┃                  永无BUG！
//    ┃　　　┗━━━┓
//    ┃　　　　　　　┣┓
//    ┃　　　　　　　┏┛
//    ┗┓┓┏━┳┓┏┛
//      ┃┫┫　┃┫┫
//      ┗┻┛　┗┻┛
/**
 * @author The one
 * @date 2019/9/27 0027
 * @describe 测试用的
 * @email 625805189@qq.com
 * @remark
 */
class TestFragment : BaseFragment() {
    //    @BindView(R.id.wave_view)
    //    WaveView mWaveView;

    lateinit var mLogin: QMUIRoundButton
    private var mTimePicker: DateTimePicker? = null

    override fun getContentViewId(): Int {
        return R.layout.fragment_test
    }

    override fun initView(rootView: View) {
        // 左右两边的内容要放在前面
        addTopBarBackBtn()
        mLogin = rootView.findViewById(R.id.btn_login)

        val builder = DateTimePicker.Builder(_mActivity)
                .setOk("确定")
                .setCancel("取消")
                .setCancelTextColor(Color.RED)
                .setOkTextColor(resources.getColor(R.color.qmui_config_color_blue))
                .setTitleTextColor(-0x666667)
                .setKeepLastSelected(true)
                .setSelectedTextColor(QMUIResHelper.getAttrColor(_mActivity, R.attr.app_skin_primary_color))
                .setShowType(DateTimePicker.ShowType.MINUTE)
        mTimePicker = DateTimePicker(_mActivity, DateTimePicker.ResultHandler { date: Date ->
            mLogin.setText(DateUtil.dateToString(date, DateUtil.YYYY_MM_DD_HH_MM_SS))
        }, getLastMonthDate(), Date(), builder)

        mLogin.setOnClickListener{
            mTimePicker!!.show(Date())
        }
//        mWaveView.setDuration(10000);
//        mWaveView.setStyle(Paint.Style.FILL);
//        mWaveView.setColor(Color.RED);
//        mWaveView.setInterpolator(new LinearOutSlowInInterpolator());
//        mWaveView.start();
    }


    /**
     * 一个月之前的Date
     */
    private fun getLastMonthDate(): Date? {
        val calendar = Calendar.getInstance()
        calendar.time = Date()
        calendar.add(Calendar.MONTH, -1)
        return calendar.time
    }

    override fun getPresenter(): BasePresenter<*>? {
        return null
    }
}