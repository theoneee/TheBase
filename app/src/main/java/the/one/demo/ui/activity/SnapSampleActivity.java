package the.one.demo.ui.activity;

//  ┏┓　　　┏┓
//┏┛┻━━━┛┻┓
//┃　　　　　　　┃
//┃　　　━　　　┃
//┃　┳┛　┗┳　┃
//┃　　　　　　　┃
//┃　　　┻　　　┃
//┃　　　　　　　┃
//┗━┓　　　┏━┛
//    ┃　　　┃                  神兽保佑
//    ┃　　　┃                  永无BUG！
//    ┃　　　┗━━━┓
//    ┃　　　　　　　┣┓
//    ┃　　　　　　　┏┛
//    ┗┓┓┏━┳┓┏┛
//      ┃┫┫　┃┫┫
//      ┗┻┛　┗┻┛

import the.one.base.ui.activity.BaseFragmentActivity;
import the.one.base.ui.fragment.BaseFragment;
import the.one.demo.ui.fragment.MzituDetailFragment;

/**
 * @author The one
 * @date 2021/2/2 0002
 * @describe TODO
 * @email 625805189@qq.com
 * @remark
 */
public class SnapSampleActivity extends BaseFragmentActivity {
    @Override
    protected BaseFragment getFirstFragment() {
        return new MzituDetailFragment();
    }
}
