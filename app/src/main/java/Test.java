//  ┏┓　　　┏┓
//┏┛┻━━━┛┻┓
//┃　　　　　　　┃
//┃　　　━　　　┃
//┃　┳┛　┗┳　┃
//┃　　　　　　　┃
//┃　　　┻　　　┃
//┃　　　　　　　┃
//┗━┓　　　┏━┛
//    ┃　　　┃                  神兽保佑
//    ┃　　　┃                  永无BUG！
//    ┃　　　┗━━━┓
//    ┃　　　　　　　┣┓
//    ┃　　　　　　　┏┛
//    ┗┓┓┏━┳┓┏┛
//      ┃┫┫　┃┫┫
//      ┗┻┛　┗┻┛

import the.one.base.util.DateUtil;

/**
 * @author The one
 * @date 2021/2/2 0002
 * @describe TODO
 * @email 625805189@qq.com
 * @remark
 */
public class Test {

    public static void main(String[] args){
        System.out.println("=============");
        System.out.println(DateUtil.dateToYMDString(DateUtil.getNearestDayDate(-7)));
        System.out.println("=============");
    }

}
