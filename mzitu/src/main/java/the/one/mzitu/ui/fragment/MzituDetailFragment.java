package the.one.mzitu.ui.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import the.one.base.Interface.IOnKeyBackClickListener;
import the.one.base.constant.DataConstant;
import the.one.base.model.PopupItem;
import the.one.base.ui.fragment.BaseImageSnapFragment;
import the.one.base.ui.presenter.BasePresenter;
import the.one.mzitu.R;
import the.one.mzitu.bean.Mzitu;
import the.one.mzitu.presenter.MzituPresenter;

public class MzituDetailFragment extends BaseImageSnapFragment<Mzitu> {

    public static MzituDetailFragment newInstance(Mzitu mzitu) {
        MzituDetailFragment fragment = new MzituDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(DataConstant.DATA, mzitu);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static MzituDetailFragment newInstance(List<Mzitu> mzitus, String url, int position, int page) {
        MzituDetailFragment fragment = new MzituDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(DataConstant.DATA, (ArrayList<? extends Parcelable>) mzitus);
        bundle.putString(DataConstant.URL, url);
        bundle.putInt(DataConstant.POSITION, position);
        bundle.putInt(DataConstant.DATA2, page);
        fragment.setArguments(bundle);
        return fragment;
    }

    private Mzitu mMzitu;
    private List<Mzitu> mMzitus;
    private int position;
    private String URL;
    private MzituPresenter mPresenter;

    private int mTotal;

    @Override
    protected void initView(View rootView) {
        Bundle bundle = getArguments();
        assert bundle != null;
        mMzitu = bundle.getParcelable(DataConstant.DATA);
        if (null == mMzitu) {
            mMzitus = bundle.getParcelableArrayList(DataConstant.DATA);
            position = bundle.getInt(DataConstant.POSITION);
            page = bundle.getInt(DataConstant.DATA2);
            URL = bundle.getString(DataConstant.URL);
        }
        super.initView(rootView);
    }

    @Override
    protected void requestServer() {
        if (null == mMzitu) {
            if (isFirst) {
                onComplete(mMzitus);
                recycleView.scrollToPosition(position);
            } else {
                mPresenter.getCategoryData(URL, page);
            }
        } else {
            mPresenter.getDetailData(mMzitu.getLink());
        }
    }

    @Override
    public BasePresenter getPresenter() {
        return mPresenter = new MzituPresenter();
    }

    @Override
    public void onComplete(List<Mzitu> data) {
        mTotal = data.size();
        super.onComplete(data);
    }

    @Override
    protected void onScrollChanged(Mzitu item, int position) {
        position++;
        if (null != mMzitu)
            mTopLayout.setTitle(position + "/" + mTotal).setTextColor(mTextColor);
    }

    @Override
    public void onVideoClick(Mzitu data) {

    }

    @Override
    public boolean onImageLongClick(Mzitu data) {
        showBottomSheetDialog(data);
        return true;
    }

    private final String TAG_SHARE = "分享";
    private final String TAG_DOWNLOAD_ALL = "下载套图";

    @Override
    protected void initSheetItems(List<PopupItem> sheetItems) {
        sheetItems.add(new PopupItem(TAG_DOWNLOAD_ALL, R.drawable.icon_more_operation_save));
        sheetItems.add(new PopupItem(TAG_SHARE, R.drawable.ic_share));
    }

    @Override
    protected void onPermissionComplete(Mzitu data, String tag, int position) {
        // 这里不能用默认的下载方式（看父类这个方法），因为要加header,否则无法下载
        boolean isShare = tag.equals(TAG_SHARE);
        showProgressDialog(0, 100, "准备中");
        progressDialog.setCanceledOnTouchOutside(false);
        // 禁止返回键
        progressDialog.setOnKeyListener(new IOnKeyBackClickListener());
        mPresenter.downloadFile(_mActivity,isShare ? position : 0, isShare,adapter.getData());
    }

}
