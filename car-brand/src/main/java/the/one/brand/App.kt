package the.one.brand

import android.app.Activity
import rxhttp.wrapper.cahce.CacheMode
import rxhttp.wrapper.param.RxHttp
import the.one.base.BaseApplication
import the.one.base.util.RxHttpManager
import the.one.brand.ui.activity.LauncherActivity


//  ┏┓　　　┏┓
//┏┛┻━━━┛┻┓
//┃　　　　　　　┃
//┃　　　━　　　┃
//┃　┳┛　┗┳　┃
//┃　　　　　　　┃
//┃　　　┻　　　┃
//┃　　　　　　　┃
//┗━┓　　　┏━┛
//    ┃　　　┃                  神兽保佑
//    ┃　　　┃                  永无BUG！
//    ┃　　　┗━━━┓
//    ┃　　　　　　　┣┓
//    ┃　　　　　　　┏┛
//    ┗┓┓┏━┳┓┏┛
//      ┃┫┫　┃┫┫
//      ┗┻┛　┗┻┛
/**
 * @author The one
 * @date 2021/2/18 0018
 * @describe TODO
 * @email 625805189@qq.com
 * @remark
 */
class App :BaseApplication(){

    override fun getStartActivity(): Class<out Activity> {
        return LauncherActivity::class.java
    }

    override fun initHttp(builder: RxHttpManager.HttpBuilder) {
        builder.cacheMode = CacheMode.READ_CACHE_FAILED_REQUEST_NETWORK
        builder.cacheValidTime = -1
        RxHttp.init(RxHttpManager.getHttpClient(builder),true)
        RxHttpManager.initCacheMode(builder)
    }

}