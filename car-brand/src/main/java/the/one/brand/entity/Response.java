package the.one.brand.entity;


/**
 * User: ljx
 * Date: 2018/10/21
 * Time: 13:16
 */
public class Response<T> {

    private int    error_code;
    private String reason;
    private T      result;

    public int getCode() {
        return error_code;
    }

    public String getMsg() {
        return reason;
    }

    public T getData() {
        return result;
    }

    public void setErrorCode(int errorCode) {
        this.error_code = errorCode;
    }

    public void setErrorMsg(String errorMsg) {
        this.reason = errorMsg;
    }

    public void setData(T data) {
        this.result = data;
    }
}
