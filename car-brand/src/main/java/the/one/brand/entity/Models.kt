package the.one.brand.entity

/**
 "id": "11",
 "series_id": "2",
 "name": "2019款 Sportback 35 TFSI 进取型 国V",
 "year": "2019",
 "peizhi": "1.4升 涡轮增压 150马力"
 */
data class Models(
        val id:String,
        val series_id:String,
        val name:String,
        val year:String,
        val peizhi:String
)