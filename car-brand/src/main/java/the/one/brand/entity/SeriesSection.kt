package the.one.brand.entity

import com.qmuiteam.qmui.widget.section.QMUISection


//  ┏┓　　　┏┓
//┏┛┻━━━┛┻┓
//┃　　　　　　　┃
//┃　　　━　　　┃
//┃　┳┛　┗┳　┃
//┃　　　　　　　┃
//┃　　　┻　　　┃
//┃　　　　　　　┃
//┗━┓　　　┏━┛
//    ┃　　　┃                  神兽保佑
//    ┃　　　┃                  永无BUG！
//    ┃　　　┗━━━┓
//    ┃　　　　　　　┣┓
//    ┃　　　　　　　┏┛
//    ┗┓┓┏━┳┓┏┛
//      ┃┫┫　┃┫┫
//      ┗┻┛　┗┻┛
/**
 * @author The one
 * @date 2021/2/19 0019
 * @describe TODO
 * @email 625805189@qq.com
 * @remark
 */
class SeriesSection(val data:Series): QMUISection.Model<SeriesSection> {

    override fun isSameItem(other: SeriesSection?): Boolean {
        return  data.levelname == other!!.data.levelname
    }

    override fun isSameContent(other: SeriesSection?): Boolean = true

    override fun cloneForDiff(): SeriesSection {
        return SeriesSection(data)
    }

}