package the.one.brand.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.lifecycle.rxLifeScope
import com.chad.library.adapter.base.BaseQuickAdapter
import rxhttp.wrapper.cahce.CacheMode
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse
import the.one.base.constant.DataConstant
import the.one.base.ui.fragment.BaseListFragment
import the.one.base.ui.presenter.BasePresenter
import the.one.brand.R
import the.one.brand.entity.Models
import the.one.brand.entity.Series
import the.one.brand.net.Url
import the.one.brand.ui.adapter.ModelsAdapter

class ModelsFragment : BaseListFragment<Models>() {

    lateinit var mSeries: Series

    companion object {
        fun newInstance(series: Series): ModelsFragment {
            val fragment = ModelsFragment()
            val bundle = Bundle()
            bundle.putParcelable(DataConstant.DATA, series)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun isNeedSpace(): Boolean = true

    override fun initView(rootView: View?) {
        mSeries = requireArguments().getParcelable(DataConstant.DATA)
        setTitleWithBackBtn(mSeries.name)
        empty_str = "此车系查无车型信息"
        super.initView(rootView)
    }

    override fun getAdapter(): BaseQuickAdapter<*, *> = ModelsAdapter()

    override fun requestServer() {
        rxLifeScope.launch(
                {
                    val data = RxHttp.get(Url.MODELS)
                            .add("series_id", mSeries.id)
                            .setCacheMode(CacheMode.READ_CACHE_FAILED_REQUEST_NETWORK)
                            .toResponse<List<Models>>()
                            .await()
                    onComplete(data)
                }, {
                onFail(it.message)
        }
        )
    }

    override fun onFirstComplete(data: MutableList<Models>?) {
        super.onFirstComplete(data)
        recycleView.setBackgroundColor(getColor(R.color.qmui_config_color_background))
        onNormal()
    }

    override fun getPresenter(): BasePresenter<*>? = null

    override fun onItemClick(adapter: BaseQuickAdapter<*, *>, view: View, position: Int) {
    }

    override fun onItemLongClick(adapter: BaseQuickAdapter<*, *>, view: View, position: Int): Boolean = false
}