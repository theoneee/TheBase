package the.one.brand.ui.activity

import android.os.Bundle
import rxhttp.wrapper.param.Param
import rxhttp.wrapper.param.RxHttp
import the.one.base.ui.activity.BaseFragmentActivity
import the.one.base.ui.fragment.BaseFragment
import the.one.brand.ui.fragment.BrandListFragment


//  ┏┓　　　┏┓
//┏┛┻━━━┛┻┓
//┃　　　　　　　┃
//┃　　　━　　　┃
//┃　┳┛　┗┳　┃
//┃　　　　　　　┃
//┃　　　┻　　　┃
//┃　　　　　　　┃
//┗━┓　　　┏━┛
//    ┃　　　┃                  神兽保佑
//    ┃　　　┃                  永无BUG！
//    ┃　　　┗━━━┓
//    ┃　　　　　　　┣┓
//    ┃　　　　　　　┏┛
//    ┗┓┓┏━┳┓┏┛
//      ┃┫┫　┃┫┫
//      ┗┻┛　┗┻┛
/**
 * @author The one
 * @date 2021/2/18 0018
 * @describe TODO
 * @email 625805189@qq.com
 * @remark
 */
class IndexActivity :BaseFragmentActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //每日访问有限制，自行到此网站上申请Key
        //https://www.juhe.cn/docs/api/id/538
        RxHttp.setOnParamAssembly { p: Param<*>? ->
            p!!.add("key", "0b6d74779b85bc36e020ab6697813714") //添加公共参数
        }
    }

    override fun getFirstFragment(): BaseFragment {
       return BrandListFragment()
    }
}