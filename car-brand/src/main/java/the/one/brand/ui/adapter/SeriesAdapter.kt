package the.one.brand.ui.adapter

import android.widget.TextView
import com.qmuiteam.qmui.widget.section.QMUISection
import the.one.base.adapter.BaseQMUISectionAdapter
import the.one.brand.R
import the.one.brand.entity.SeriesSection

class SeriesAdapter( layout:Int = R.layout.item_series): BaseQMUISectionAdapter<SeriesSection,SeriesSection>(R.layout.item_series_head,layout) {

    override fun onBindSectionHeader(holder: ViewHolder?, position: Int, section: QMUISection<SeriesSection, SeriesSection>?) {
        holder!!.itemView.apply {
            findViewById<TextView>(R.id.tv_level_name)!!.text = section!!.header.data.levelname
        }
    }

    override fun onBindSectionItem(holder: ViewHolder?, position: Int, section: QMUISection<SeriesSection, SeriesSection>?, itemIndex: Int) {
        holder!!.itemView.apply {
            findViewById<TextView>(R.id.tv_name)!!.text = section!!.getItemAt(itemIndex).data.name
            findViewById<TextView>(R.id.tv_sname)!!.text = section!!.getItemAt(itemIndex).data.sname
        }
    }

}