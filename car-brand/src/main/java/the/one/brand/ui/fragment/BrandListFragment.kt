package the.one.brand.ui.fragment

import androidx.lifecycle.rxLifeScope
import com.hjq.permissions.OnPermission
import com.hjq.permissions.Permission
import com.hjq.permissions.XXPermissions
import rxhttp.wrapper.cahce.CacheMode
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse
import the.one.base.ui.fragment.BaseLetterSearchFragment
import the.one.base.ui.presenter.BasePresenter
import the.one.brand.R
import the.one.brand.entity.Brand
import the.one.brand.entity.BrandSection
import the.one.brand.net.Url


//  ┏┓　　　┏┓
//┏┛┻━━━┛┻┓
//┃　　　　　　　┃
//┃　　　━　　　┃
//┃　┳┛　┗┳　┃
//┃　　　　　　　┃
//┃　　　┻　　　┃
//┃　　　　　　　┃
//┗━┓　　　┏━┛
//    ┃　　　┃                  神兽保佑
//    ┃　　　┃                  永无BUG！
//    ┃　　　┗━━━┓
//    ┃　　　　　　　┣┓
//    ┃　　　　　　　┏┛
//    ┗┓┓┏━┳┓┏┛
//      ┃┫┫　┃┫┫
//      ┗┻┛　┗┻┛
/**
 * @author The one
 * @date 2021/2/18 0018
 * @describe TODO
 * @email 625805189@qq.com
 * @remark
 */
class BrandListFragment : BaseLetterSearchFragment<BrandSection>() {

    override fun getPresenter(): BasePresenter<*>? = null

    override fun getTitleString(): String = getString(R.string.app_name)

    override fun isNeedMultiChoose(): Boolean = false

    override fun setTitleWithBackBtn(title: String?) {
        mTopLayout.setTitle(title)
    }

    override fun isUseColorId(): Boolean = false

    override fun onLazyInit() {
        XXPermissions.with(_mActivity)
                .permission(Permission.Group.STORAGE)
                .request(object : OnPermission {
                    override fun hasPermission(granted: List<String>, all: Boolean) {
                        if (all) {
                            request()
                        } else {
                            onLazyInit()
                        }
                    }

                    override fun noPermission(denied: List<String>, quick: Boolean) {
                        if (quick) { // 如果是被永久拒绝就跳转到应用权限系统设置页面
                            showToast("权限被禁止，请在设置里打开权限。")
                        } else {
                            onLazyInit()
                        }
                    }
                })
    }

    fun request() {
        showLoadingPage()
        rxLifeScope.launch({
            val list = RxHttp.get(Url.BRAND)
                    .setCacheMode(CacheMode.READ_CACHE_FAILED_REQUEST_NETWORK)
                    .toResponse<List<Brand>>()
                    .await()
            onBrandListSuccess(list)
        }, {
            showEmptyPage(it.message)
        }
        )
    }

    private fun onBrandListSuccess(response: List<Brand>?) {
        if (null == response) {
            showEmptyPage("空")
            return
        }
        val items = mutableListOf<BrandSection>()
        for (re in response) {
            items.add(BrandSection(re))
        }
        mAdapter.setData(parseSectionList(items, null))
        mAdapter.setSelectsMap(mDataMap)
        showView(sideLetterBar, flTopLayout)
        showContentPage()
    }

    override fun onItemClick(t: BrandSection) {
        startFragment(SeriesListFragment.newInstance(t.brand))
    }

    override fun onConfirmSelect(selects: MutableList<BrandSection>?) {

    }

}