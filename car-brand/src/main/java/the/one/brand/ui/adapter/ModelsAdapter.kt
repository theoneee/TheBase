package the.one.brand.ui.adapter

import the.one.base.adapter.TheBaseQuickAdapter
import the.one.base.adapter.TheBaseViewHolder
import the.one.brand.R
import the.one.brand.entity.Models

class ModelsAdapter(layout:Int = R.layout.item_models): TheBaseQuickAdapter<Models>(layout) {

    override fun convert(holder: TheBaseViewHolder, item: Models) {
        setRadiusAndShadow(holder,R.id.rl_parent)
        holder.run {
            setText(R.id.tv_name,item.name)
            setText(R.id.tv_peizhi,item.peizhi)
        }
    }

}