package the.one.brand.net

import rxhttp.wrapper.annotation.DefaultDomain
import the.one.brand.entity.Level


//  ┏┓　　　┏┓
//┏┛┻━━━┛┻┓
//┃　　　　　　　┃
//┃　　　━　　　┃
//┃　┳┛　┗┳　┃
//┃　　　　　　　┃
//┃　　　┻　　　┃
//┃　　　　　　　┃
//┗━┓　　　┏━┛
//    ┃　　　┃                  神兽保佑
//    ┃　　　┃                  永无BUG！
//    ┃　　　┗━━━┓
//    ┃　　　　　　　┣┓
//    ┃　　　　　　　┏┛
//    ┗┓┓┏━┳┓┏┛
//      ┃┫┫　┃┫┫
//      ┗┻┛　┗┻┛
/**
 * @author The one
 * @date 2021/2/18 0018
 * @describe 接口
 * @email 625805189@qq.com
 * @remark
 */
object Url {

    @JvmField
    @DefaultDomain //设置为默认域名
    var baseUrl = "http://apis.juhe.cn/cxdq/"

    /**
     * 请求示例：http://apis.juhe.cn/cxdq/brand?key=xxxx&first_letter=A
     * 接口备注：返回车辆品牌所有列表，或更具中文拼音首字母查询品牌列表
     */
    const val BRAND = "brand"

    /**
     *  请求示例：http://apis.juhe.cn/cxdq/series?key=xxxx&brandid=1&levelid=5
     *  接口备注：根据车辆品牌ID查询旗下车系列表
     */
    const val SERIES = "series"

    /**
     * 请求示例：http://apis.juhe.cn/cxdq/models?key=xxxxx&series_id=2
     * 接口备注：根据车系id查询旗下车型列表
     */
    const val MODELS = "models"



    /** 1:微型车,2:小型车,3:紧凑型车,4:中型车,5:中大型车,6:大型车,7:跑车,
     *  8:MPV,11:微面,12:微卡,13:轻客,14:低端皮卡,15:高端皮卡,16:小型SUV,
     *  17:紧凑型SUV,18:中型SUV,19:中大型SUV,20:大型SUV,21:紧凑型MPV
     */
    fun getLevels():List<Level>{
        val list = mutableListOf<Level>()
        list.add(Level(1, "微型车"))
        list.add(Level(2, "小型车"))
        list.add(Level(3, "紧凑型车"))
        list.add(Level(4, "中型车"))
        list.add(Level(5, "中大型车"))
        list.add(Level(6, "大型车"))
        list.add(Level(7, "跑车"))
        list.add(Level(8, "MPV"))
        list.add(Level(11, "微面"))
        list.add(Level(12, "微卡"))
        list.add(Level(13, "轻客"))
        list.add(Level(14, "低端皮卡"))
        list.add(Level(15, "高端皮卡"))
        list.add(Level(16, "小型SUV"))
        list.add(Level(17, "紧凑型SUV"))
        list.add(Level(18, "中型SUV"))
        list.add(Level(19, "中大型SUV"))
        list.add(Level(20, "大型SUV"))
        list.add(Level(21, "紧凑型MPV"))
        return list
    }

}